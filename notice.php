<?php include 'includes/header.php';?>
<?php include 'includes/navbar.php';?>
       
<section class="notice-wrapper">
    <div class="container">
        <div class="heading-text">
            <h1>Notice List</h1>
        </div>
    </div>
</section>  
<section class="notice-list-wrapper">
<div class="container">
<div class="row">
    <div class="col-md-4">
        <div class="notice-filter">
            <div class="filter-heading">
                <p>Filter Notice <span class="pull-right ion-chevron-down"></span></p>
            </div>
            <div class="filter-body">
             <div class="form-wrapper">
                            <form action="">
                                <div class="form-group">
                                <div class="checkbox">
                                <label><input type="checkbox" value="">all</label>
                                </div>
                                <div class="checkbox">
                                <label><input type="checkbox" value="">Educational Sector</label>
                                </div>
                                <div class="checkbox">
                                <label><input type="checkbox" value="">Health Post</label>
                                </div>
                                <div class="checkbox">
                                <label><input type="checkbox" value="">Officers</label>
                                </div>
                                <div class="checkbox">
                                <label><input type="checkbox" value="">Population</label>
                                </div>
                                <div class="checkbox">
                                <label><input type="checkbox" value="">Houses landlord</label>
                                </div>
                                <div class="checkbox">
                                <label><input type="checkbox" value="">Resource of water</label>
                                </div>
                                <div class="checkbox">
                                <label><input type="checkbox" value="">carrers</label>
                                </div>
                                </div>
                            </form>
                            <ul class="list-inline">
                <li><a href="" class="btn btn-outline-primary"><span class="ion-android-close"></span> clear selected</a></li>
            </ul>
                        </div>
            </div>
            
            </div>
    </div>
    <div class="col-md-8">
        <div class="search-data">
            <form action="">
            <div class="row no-gutters">
                <div class="col-md-9">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search Here..">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <button class="btn btn-outline-search"> Search </button>
                    </div>
                </div>
            </div>
            </form>
        </div>
        <div class="notice-data-blog">
            <div class="notice-data">
            <h4>Notice Title Goes Here</h4>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi minus recusandae dolore itaque fugit blanditiis ipsa fuga eius eveniet dolor asperiores, quas voluptatum ducimus delectus, non, repellendus ab nam optio.</p>
            <ul class="list-unstyled">
                <li><i>published on 24th oct 2018</i><span class="pull-right"><a href="">Read More</a></span></li>
            </ul>
            </div>
        </div>
        <div class="notice-data-blog">
            <div class="notice-data">
            <h4>Notice Title Goes Here</h4>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi minus recusandae dolore itaque fugit blanditiis ipsa fuga eius eveniet dolor asperiores, quas voluptatum ducimus delectus, non, repellendus ab nam optio.</p>
            <ul class="list-unstyled">
                <li><i>published on 24th oct 2018</i><span class="pull-right"><a href="">Read More</a></span></li>
            </ul>
            </div>
        </div>
        <div class="notice-data-blog">
            <div class="notice-data">
            <h4>Notice Title Goes Here</h4>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi minus recusandae dolore itaque fugit blanditiis ipsa fuga eius eveniet dolor asperiores, quas voluptatum ducimus delectus, non, repellendus ab nam optio.</p>
            <ul class="list-unstyled">
                <li><i>published on 24th oct 2018</i><span class="pull-right"><a href="">Read More</a></span></li>
            </ul>
            </div>
        </div>
        <div class="notice-data-blog">
            <div class="notice-data">
            <h4>Notice Title Goes Here</h4>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi minus recusandae dolore itaque fugit blanditiis ipsa fuga eius eveniet dolor asperiores, quas voluptatum ducimus delectus, non, repellendus ab nam optio.</p>
            <ul class="list-unstyled">
                <li><i>published on 24th oct 2018</i><span class="pull-right"><a href="">Read More</a></span></li>
            </ul>
            </div>
        </div>
        <div class="notice-data-blog">
            <div class="notice-data">
            <h4>Notice Title Goes Here</h4>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi minus recusandae dolore itaque fugit blanditiis ipsa fuga eius eveniet dolor asperiores, quas voluptatum ducimus delectus, non, repellendus ab nam optio.</p>
            <ul class="list-unstyled">
                <li><i>published on 24th oct 2018</i><span class="pull-right"><a href="">Read More</a></span></li>
            </ul>
            </div>
        </div>
        <div class="pagination-wrapper text-center">
                            <nav aria-label="...">
                                <ul class="pagination">
                                    <li class="active"><a href="?p=0" data-original-title="" title="">1</a></li> 
                                    <li><a href="?p=1" data-original-title="" title="">2</a></li> 
                                    <li><a href="?p=1" data-original-title="" title="">3</a></li> 
                                    <li><a href="?p=1" data-original-title="" title="">4</a></li> 
                                </ul>
                            </nav>
        </div>
    </div>
</div>
</div>
</section>

<?php include 'includes/footer.php';?>

        