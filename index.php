<?php include 'includes/header.php';?>
<?php include 'includes/navbar.php';?>
        <section class="heading-text">
            <div class="container">
                <div class="section-wrapper-padding">
                    <div class="row">
                        <div class="col-md-6">
                            <h1>
                                Gis <span>System</span> 
                            </h1>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas nam ullam nihil id delectus dignissimos dolore aliquam veritatis.
                            </p>
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="heading-button-wrapper">
                                <a href="" class="btn btn-outline-primary"><span><img src="assets/svg/icon-pdf.svg" alt="pdf"></span> Download</a>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </section>

        <section class="map">
            <div class="embed-responsive embed-responsive-100x400px">
                <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d3532.1855782835787!2d85.32911900000151!3d27.711555900013337!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2snp!4v1548994733904" width="" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </section>

        <section class="main-element-gis">
            <div class="container">
                
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <form action="" class="gis-form-wrapper">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <select class="form-control" id="sel1">
                                                <option>Filter <span class="caret"></span> </option>
                                                <option>Ward No 2</option>
                                                <option>Ward No 3</option>
                                                <option>Ward No 3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group" id="sell2">
                                            <input type="text" class="form-control" placeholder="Search here">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="block-component-wrapper">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="block-component">
                                    <ul class="list-inline">
                                        <li><span><img src="assets/image/ward-img.jpg" alt=""></span></li>
                                        <li class="pull-right"> <i>published on 24 November</i> </li>
                                    </ul>
                                    <h4>
                                        Ward Title Goes Here
                                    </h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam iusto atque autem laborum non dolores adipisci veritatis nihil! Expedita soluta aliquid vero sapiente incidunt porro repudiandae ad numquam corporis dignissimos?</p>
                                    <a href="" data-toggle="modal" data-target="#modalView"></a>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="block-component">
                                    <ul class="list-inline">
                                        <li><span><img src="assets/image/ward-img.jpg" alt=""></span></li>
                                        <li class="pull-right"> <i>published on 24 November</i> </li>
                                    </ul>
                                    <h4>
                                        Ward Title Goes Here
                                    </h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam iusto atque autem laborum non dolores adipisci veritatis nihil! Expedita soluta aliquid vero sapiente incidunt porro repudiandae ad numquam corporis dignissimos?</p>
                                    <a href=""></a>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="block-component">
                                    <ul class="list-inline">
                                        <li><span><img src="assets/image/ward-img.jpg" alt=""></span></li>
                                        <li class="pull-right"> <i>published on 24 November</i> </li>
                                    </ul>
                                    <h4>
                                        Ward Title Goes Here
                                    </h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam iusto atque autem laborum non dolores adipisci veritatis nihil! Expedita soluta aliquid vero sapiente incidunt porro repudiandae ad numquam corporis dignissimos?</p>
                                    <a href=""></a>
                                </div>
                            </div> 
                            <div class="col-md-4">
                                <div class="block-component">
                                    <ul class="list-inline">
                                        <li><span><img src="assets/image/ward-img.jpg" alt=""></span></li> 
                                        <li class="pull-right"> <i>published on 24 November</i> </li>
                                    </ul>
                                    <h4>
                                        Ward Title Goes Here
                                    </h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam iusto atque autem laborum non dolores adipisci veritatis nihil! Expedita soluta aliquid vero sapiente incidunt porro repudiandae ad numquam corporis dignissimos?</p>
                                    <a href=""></a>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="block-component">
                                    <ul class="list-inline">
                                        <li><span><img src="assets/image/ward-img.jpg" alt=""></span></li>
                                        <li class="pull-right"> <i>published on 24 November</i> </li>
                                    </ul>
                                    <h4>
                                        Ward Title Goes Here
                                    </h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam iusto atque autem laborum non dolores adipisci veritatis nihil! Expedita soluta aliquid vero sapiente incidunt porro repudiandae ad numquam corporis dignissimos?</p>
                                    <a href=""></a>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="block-component">
                                    <ul class="list-inline">
                                        <li><span><img src="assets/image/ward-img.jpg" alt=""></span></li>
                                        <li class="pull-right"> <i>published on 24 November</i> </li>
                                    </ul>
                                    <h4>
                                        Ward Title Goes Here
                                    </h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam iusto atque autem laborum non dolores adipisci veritatis nihil! Expedita soluta aliquid vero sapiente incidunt porro repudiandae ad numquam corporis dignissimos?</p>
                                    <a href=""></a>
                                </div>
                            </div>       
                        </div>
                        <div class="pagination-wrapper text-center">
                            <nav aria-label="...">
                                <ul class="pagination">
                                    <li class="active"><a href="?p=0" data-original-title="" title="">1</a></li> 
                                    <li><a href="?p=1" data-original-title="" title="">2</a></li> 
                                    <li><a href="?p=1" data-original-title="" title="">3</a></li> 
                                    <li><a href="?p=1" data-original-title="" title="">4</a></li> 
                                </ul>
                            </nav>
                        </div>
                        
                    </div>
            </div>
        </section>

        <section class="about-us">
            <div class="about-us-wrapper">
                    <div class="row">
                    <div class="col-md-8">
                            <div class="about-us-text-wrapper">
                                <div class="container">
                                    <h1>
                                    <span>About</span> Us
                                    </h1>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas nam ullam nihil id delectus dignissimos dolore aliquam veritatis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo praesentium velit necessitatibus ullam neque corporis odio aspernatur molestiae placeat iure iste distinctio fugit maiores blanditiis perspiciatis, sunt, cumque minima dolorum.
                            </p>
                            <a href="">Know More</a>
                                </div>
                            </div>
                    </div>
                    <div class="col-md-4">
                        <div class="image-slider">
                            <div class="image">
                                <img src="assets/image/slide-1.jpg" alt="" class="img-responsive">
                            </div>
                            <div class="image">
                                <img src="assets/image/slide-2.jpg" alt="" class="img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
                </div>   
        </section>

        <section class="count-data-wrapper">
             <div class="count-wrapper">
                    <div class="bg-count-primary"></div>
                        <div class="container bg-data">
                            <div class="row">
                            <div class="col-md-4">
                                <ul class="list-unstyled">
                                    <li class="big-prop">10,457</li>
                                    <li class="small-prop">Population Counted</li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                 <ul class="list-unstyled">
                                    <li class="big-prop">1,757</li>
                                    <li class="small-prop">Renowned land owner</li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                 <ul class="list-unstyled">
                                    <li class="big-prop">57</li>
                                    <li class="small-prop">Health Offices</li>
                                </ul>
                            </div>
                             </div>
                        </div>                        
                </div>
        </section>

        <section class="latest-thing-blog">
            <div class="container">
                <div class="text-center">
                    <h1>
                                Latest  <span> Things </span> Going 
                            </h1>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut reprehenderit ipsam cupiditate minima sit laborum esse, fuga eligendi suscipit quo, ratione quas saepe inventore a nisi quia quidem adipisci iste! Lorem ipsum dolor sit amet consectetur adipisicing elit. In velit illum fugit quis. Ratione quae dolorum dolores, possimus a sit molestias nobis voluptatem temporibus voluptate sequi nemo. Voluptas, rerum similique?
                            </p>
                </div>
                <div class="blog-slider-wrapper">
                    <div class="blog-slider">
                        <div class="blog">
                            <small><i>published on 13 November</i></small>
                            <h4>Title of the News</h4>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis reprehenderit molestias sunt ex iure optio amet velit delectus ab nemo voluptatum soluta ullam eum, modi quis repellendus, eligendi dolores libero?</p>
                            <a href="">Read More <span class="ion-chevron-right"></span></a>
                        </div>
                        <div class="blog">
                            <small><i>published on 13 November</i></small>
                            <h4>Title of the News</h4>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis reprehenderit molestias sunt ex iure optio amet velit delectus ab nemo voluptatum soluta ullam eum, modi quis repellendus, eligendi dolores libero?</p>
                            <a href="">Read More <span class="ion-chevron-right"></span></a>
                        </div>
                        <div class="blog">
                            <small><i>published on 13 November</i></small>
                            <h4>Title of the News</h4>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis reprehenderit molestias sunt ex iure optio amet velit delectus ab nemo voluptatum soluta ullam eum, modi quis repellendus, eligendi dolores libero?</p>
                            <a href="">Read More <span class="ion-chevron-right"></span></a>
                        </div>
                        <div class="blog">
                            <small><i>published on 13 November</i></small>
                            <h4>Title of the News</h4>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis reprehenderit molestias sunt ex iure optio amet velit delectus ab nemo voluptatum soluta ullam eum, modi quis repellendus, eligendi dolores libero?</p>
                            <a href="">Read More <span class="ion-chevron-right"></span></a>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
        

<?php include 'includes/footer.php';?>
    <script>
        $('.image-slider').slick({
        
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 2000,
        fade: true,
        cssEase: 'linear'
        });
        </script>

        <script>
        $('.blog-slider').slick({
        infinite: true,
        slidesToShow: 2, 
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        });
        </script>

<?php include 'includes/modals.php';?>

        