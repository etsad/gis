<?php include 'includes/header.php';?>
<?php include 'includes/navbar.php';?>
<section class="gallery-bg">
<div class="container">
    <h1>Welcome to gallery</h1>
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia voluptatibus ab voluptate incidunt soluta, autem repudiandae qui perspiciatis odit consequatur vitae, harum commodi natus quibusdam ex recusandae praesentium quaerat aliquam?</p>
    
</div>
</section>     
<section class="gallery-body">
        <div class="container">
        <div class="tab-wrapper">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">Images</a></li>
                    <li><a data-toggle="tab" href="#menu1">Videos</a></li>
                </ul>
            
            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <div class="row no-gutters">
                        <div class="col-md-8">
                        <a href="assets/image/slide-1.jpg" data-lightbox="roadtrip"><img src="assets/image/slide-1.jpg" alt="" class="img-responsive"></a>
                        </div>
                        <div class="col-md-4">
                        <a href="assets/image/slide-2.jpg" data-lightbox="roadtrip"><img src="assets/image/slide-2.jpg" alt="" class="img-responsive"></a>
                        </div>
                        <div class="col-md-4">
                        <a href="assets/image/slide-2.jpg" data-lightbox="roadtrip"><img src="assets/image/slide-2.jpg" alt="" class="img-responsive"></a>
                        </div>
                        <div class="col-md-4">
                        <a href="assets/image/slide-2.jpg" data-lightbox="roadtrip"><img src="assets/image/slide-2.jpg" alt="" class="img-responsive"></a>
                        </div>
                        <div class="col-md-4">
                        <a href="assets/image/slide-2.jpg" data-lightbox="roadtrip"><img src="assets/image/slide-2.jpg" alt="" class="img-responsive"></a>
                        </div>
                    </div>
                </div>
                <div id="menu1" class="tab-pane fade">
                <video width="100%" controls>
                            <source src="//player.vimeo.com/video/304005808?title=0&portrait=0&byline=0&autoplay=1" type="video/mp4">
                            Your browser does not support HTML5 video.
                        </video>
                        <div class="row no-gutters">
                            <div class="col-md-6">
                            <video width="100%" controls>
                            <source src="//player.vimeo.com/video/304005808?title=0&portrait=0&byline=0&autoplay=1" type="video/mp4">
                            Your browser does not support HTML5 video.
                        </video>
                            </div>
                            <div class="col-md-6">
                            <video width="100%" controls>
                            <source src="//player.vimeo.com/video/304005808?title=0&portrait=0&byline=0&autoplay=1" type="video/mp4">
                            Your browser does not support HTML5 video.
                        </video>
                            </div>
                        </div>
                        
                </div>
                </div>
                </div>
        </div>
</section>

<?php include 'includes/footer.php';?>

        