<?php include 'includes/header.php';?>
<?php include 'includes/navbar.php';?>
       
<section class="map-wrapper">
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-md-3">
                <div class="map-sidebar">
                        <h5>View on map</h5>
                        <div class="dropdown">
                            <button class="btn-outline-white dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Choose Ward
                                <span class="ion-chevron-down"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                        <div class="form-wrapper">
                            <form action="">
                                <p>Filter View Control </p>
                                <div class="form-group">
                                <div class="checkbox">
                                <label><input type="checkbox" value="">all</label>
                                </div>
                                <div class="checkbox">
                                <label><input type="checkbox" value="">Educational Sector</label>
                                </div>
                                <div class="checkbox">
                                <label><input type="checkbox" value="">Health Post</label>
                                </div>
                                <div class="checkbox">
                                <label><input type="checkbox" value="">Officers</label>
                                </div>
                                <div class="checkbox">
                                <label><input type="checkbox" value="">Population</label>
                                </div>
                                <div class="checkbox">
                                <label><input type="checkbox" value="">Houses landlord</label>
                                </div>
                                <div class="checkbox">
                                <label><input type="checkbox" value="">Resource of water</label>
                                </div>
                                <div class="checkbox">
                                <label><input type="checkbox" value="">carrers</label>
                                </div>
                                </div>
                            </form>
                        </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="map-view-wrapper">
                    <div class="embed-responsive embed-responsive-100x400px">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d3532.1855782835787!2d85.32911900000151!3d27.711555900013337!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2snp!4v1548994733904" width="" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                    <div class="map-search">
                    <form action="" class="gis-form-wrapper">
                    <div class="form-group" id="sell2">
                                            <input type="text" class="form-control" placeholder="Search here">
                                        </div>
                            </form>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</section>    

<?php include 'includes/footer.php';?>

        