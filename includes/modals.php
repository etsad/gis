<!-- Modal Search -->
<div id="modalView" class="modal modal-view" data-easein="slideUpBigIn"  tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true"> 
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        
        <div class="row">
          <div class="col-md-8">
              <div class="modal-con-wrapper">
                <figure>
                  <img src="assets/image/ward-img.jpg" alt="">
                </figure>
                <ul class="list-unstyled">
                  <li class="small-prop"><i>Published On May 23</i></li>
                  <li class="big-prop">Ward title goes here</li>
                </ul>
                <p>Vivamus eget aliquam dui. Integer eu arcu vel arcu suscipit 
                ultrices quis non mauris. Aenean scelerisque, sem eu dictum 
                commodo, velit nisi blandit magna, quis scelerisque ipsum 
                lectus ut libero. Sed elit diam, dignissim ac congue quis, 
                aliquam in purus. Proin ligula nulla, scelerisque quis venenatis 
                pulvinar, congue eget neque. Proin scelerisque metus sit amet 
                dolor tempor vehicula. Sed laoreet quis velit vitae facilisis. 
                Duis ut sapien eu urna laoreet maximus. Donec nibh diam, 
                vulputate vel nulla ut, viverra congue turpis. Fusce consectetur 
                posuere purus, eget placerat nunc hendrerit at. Sed lectus dui, 
                euismod a odio vitae, dictum dictum justo. </p>
                <a href="" class="btn btn-outline-primary "><span><img src="assets/svg/print.svg" alt=""></span> Print Document</a>
              </div>
          </div>
          <div class="col-md-4">
            <div class="embed-responsive embed-responsive-100x200px">
                <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d3532.1855782835787!2d85.32911900000151!3d27.711555900013337!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2snp!4v1548994733904" width="" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
</div>