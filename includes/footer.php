 <footer class="footer">
    <div class="container">
      <p>Copyright © 2019 company name. All rights reserved</p>
    </div>
 </footer>



 <!-- jQuery first -->
 <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
    <!-- bootstrap -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Lightbox -->
    <script src="vendor/lightbox2-master/dist/js/lightbox.js"></script>
     <!-- slick slider-->
     <script src="vendor/slick/slick/slick.min.js"></script> 
    
    <!-- custom javascript-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.ui.min.js"></script>
    <script src="assets/js/custom.js"></script>
   
    
    
  </body>
</html>

