<nav class="navbar navbar-default custom-navbar navbar-fixed-top">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <ul class="list-inline">
          <li>
            <a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample"><img src="assets/svg/news-tiles.svg" alt="news"></a>
          </li>
          <li class="logo">
            <a href="index.php"><strong>Gis</strong> Logo</a>
          </li>
        </ul>
      </div>
      <div class="col-md-6 text-right">
        <ul class="list-inline">
          <li><a href="contact.php"><span><img src="assets/svg/contact.svg" alt=""></span>Contact</a></li>
          <li><a href=""><span><img src="assets/svg/share.svg" alt=""></span></span>Share </a></li> 
        </ul>  
      </div>
    </div>
  </div>
  <div class="collapse collapse-sec" id="collapseExample">
  <div class="card">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-4">
            <div class="card-body">
              <h4>News Title appears here</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste dicta dolores velit perferendis unde, culpa eveniet?</p>
              <a href="#"></a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card-body">
              <h4>News Title appears here</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste dicta dolores velit perferendis unde, culpa eveniet?</p>
              <a href="#"></a>
            </div>
          </div>
           <div class="col-md-4">
            <div class="card-body">
              <h4>News Title appears here</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste dicta dolores velit perferendis unde, culpa eveniet?</p>
              <a href="#"></a>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="card-body">
              <h4>News Title appears here</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste dicta dolores velit perferendis unde, culpa eveniet?</p>
              <a href="#"></a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card-body">
              <h4>News Title appears here</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste dicta dolores velit perferendis unde, culpa eveniet?</p>
              <a href="#"></a>
            </div>
          </div>
           
        </div>
      </div>
  </div>
</div>
</nav>
