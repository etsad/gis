<?php include 'includes/header.php';?>
<?php include 'includes/navbar.php';?>
       
<section class="notice-wrapper">
    <div class="container">
        <div class="heading-text">
            <h1>News & Updates</h1>
        </div>
    </div>
</section>  
<section class="notice-list-wrapper">
<div class="container">
    <div class="news-praper">
        <div class="notice-data-blog news">
            <div class="row">
                <div class="col-md-4">
                    <figure>
                        <img src="assets/image/nepal-news.jpg" alt="" class="img-responsive">
                    </figure>
                </div>
                <div class="col-md-8">
                    <div class="news-point">
                        <h4>Notice Title Goes Here</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi minus recusandae dolore itaque fugit blanditiis ipsa fuga eius eveniet dolor asperiores, quas voluptatum ducimus delectus, non, repellendus ab nam optio.</p>
                        <ul class="list-unstyled">
                            <li><i>published on 24th oct 2018</i><span class="pull-right"><a href="news-details.php">Read More</a></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="notice-data-blog news">
            <div class="row">
                <div class="col-md-4">
                    <figure>
                        <img src="assets/image/nepal-news.jpg" alt="" class="img-responsive">
                    </figure>
                </div>
                <div class="col-md-8">
                    <div class="news-point">
                        <h4>Notice Title Goes Here</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi minus recusandae dolore itaque fugit blanditiis ipsa fuga eius eveniet dolor asperiores, quas voluptatum ducimus delectus, non, repellendus ab nam optio.</p>
                        <ul class="list-unstyled">
                            <li><i>published on 24th oct 2018</i><span class="pull-right"><a href="news-details.php">Read More</a></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="notice-data-blog news">
            <div class="row">
                <div class="col-md-4">
                    <figure>
                        <img src="assets/image/nepal-news.jpg" alt="" class="img-responsive">
                    </figure>
                </div>
                <div class="col-md-8">
                    <div class="news-point">
                        <h4>Notice Title Goes Here</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi minus recusandae dolore itaque fugit blanditiis ipsa fuga eius eveniet dolor asperiores, quas voluptatum ducimus delectus, non, repellendus ab nam optio.</p>
                        <ul class="list-unstyled">
                            <li><i>published on 24th oct 2018</i><span class="pull-right"><a href="news-details.php">Read More</a></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="notice-data-blog news">
            <div class="row">
                <div class="col-md-4">
                    <figure>
                        <img src="assets/image/nepal-news.jpg" alt="" class="img-responsive">
                    </figure>
                </div>
                <div class="col-md-8">
                    <div class="news-point">
                        <h4>Notice Title Goes Here</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi minus recusandae dolore itaque fugit blanditiis ipsa fuga eius eveniet dolor asperiores, quas voluptatum ducimus delectus, non, repellendus ab nam optio.</p>
                        <ul class="list-unstyled">
                            <li><i>published on 24th oct 2018</i><span class="pull-right"><a href="news-details.php">Read More</a></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pagination-wrapper text-center">
                            <nav aria-label="...">
                                <ul class="pagination">
                                    <li class="active"><a href="?p=0" data-original-title="" title="">1</a></li> 
                                    <li><a href="?p=1" data-original-title="" title="">2</a></li> 
                                    <li><a href="?p=1" data-original-title="" title="">3</a></li> 
                                    <li><a href="?p=1" data-original-title="" title="">4</a></li> 
                                </ul>
                            </nav>
        </div>
</div>
</section>

<?php include 'includes/footer.php';?>

        