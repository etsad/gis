<?php include 'includes/header.php';?>
<?php include 'includes/navbar.php';?>
       
<section class="notice-wrapper notice-details">
    <div class="container">
        <div class="heading-text">
            <h2>News Title Goes Here</h2>
            <ul class="list-inline">
                <li><i>Published On 24th October 2018</i></li>
                <li class="pull-right"><a href="" class="btn btn-outline-white"> Download News  </a></li>
            </ul>
        </div>
    </div>
</section>  

<div class="notice-section">
    <div class="container">
    <div class="notice-data-blog">
        <a href="news.php" class="back"><span class="ion-android-arrow-up-left"></span> Back to news</a>
        <div class="news-main-img">
        <a href="assets/image/nepal-news.jpg" data-lightbox="roadtrip"><img src="assets/image/nepal-news.jpg" alt="" class="img-responsive"></a>
        </div>
        <div class="notice-data">
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Odio dignissimos ipsa ducimus fugiat, eos numquam natus quod unde! Rem fugit numquam id cupiditate dicta officiis blanditiis quo molestiae nisi fuga? Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum accusantium illum velit nobis sit ducimus possimus quaerat eius eveniet. Dolorem voluptatum quidem velit perspiciatis obcaecati hic vero. Quam, non odio? Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem iste ipsa incidunt eveniet fugit dignissimos suscipit itaque, est, aspernatur adipisci ducimus harum laborum natus repellendus recusandae quos corrupti quis non.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem officiis voluptates atque iste, tempora placeat! Aspernatur voluptatibus ad totam dolor ducimus blanditiis similique dolorum ipsa? Modi incidunt ducimus ullam aut.</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus blanditiis ipsam facilis impedit unde accusamus at eligendi! Autem sint aperiam deserunt ratione eum temporibus quibusdam suscipit, doloremque saepe sed beatae!</p>

            
            <div class="media-phase">
            <h3>More Media</h3>
            <div class="row">
                        <div class="col-md-3">
                        <a href="assets/image/slide-1.jpg" data-lightbox="roadtrip"><img src="assets/image/slide-1.jpg" alt="" class="img-responsive"></a>
                        </div>
                        <div class="col-md-3">
                        <a href="assets/image/slide-2.jpg" data-lightbox="roadtrip"><img src="assets/image/slide-2.jpg" alt="" class="img-responsive"></a>
                        </div>
                        <div class="col-md-3">
                        <a href="assets/image/slide-2.jpg" data-lightbox="roadtrip"><img src="assets/image/slide-2.jpg" alt="" class="img-responsive"></a>
                        </div>
                        <div class="col-md-3">
                        <a href="assets/image/slide-2.jpg" data-lightbox="roadtrip"><img src="assets/image/slide-2.jpg" alt="" class="img-responsive"></a>
                        </div>
            </div>
            </div>

            <div class="source">
                <ul class="list-inline">
                    <li><a href="" class="btn btn-outline-primary"><span class="ion-chevron-left"></span> Prev</a></li>
                    <li><a href="" class="btn btn-outline-primary"> Next <span class="ion-chevron-right"></span></a></li>
                </ul>
            </div>
            
        </div>
    </div>
    </div>
</div>

<?php include 'includes/footer.php';?>

        