<?php include 'includes/header.php';?>
<?php include 'includes/navbar.php';?>
       
<section class="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
               
            </div>
            <div class="col-md-8">
            <div class="contact-wrapper">
            <div class="form-wrapper">
                <form action="">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">First Name</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Last Name</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Your Email </label>
                        <input type="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Your Message </label>
                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary-gis"><span class="pull-right ion-chevron-right"></span>    Submit Now</button>
                    </div>
                </form>
            </div> 
        </div>
            </div>
        </div>
        
    </div>
</section>  
<section class="map-contact-section">
                    <div class="embed-responsive embed-responsive-100x400px">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d3532.1855782835787!2d85.32911900000151!3d27.711555900013337!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2snp!4v1548994733904" width="" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
</section> 

<section class="extra-contact">
    <div class="container">
        <div class="contact-din-one">
            <div class="row">
                <div class="col-md-4">
                    <ul class="list-unstyled">
                        <li><strong>Address Name , Location</strong></li>
                        <li><span>P:</span> +092738237 , +092738237</li>
                        <li><span>E:</span> <a href=""> mail@mail.com</a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <div class="text-center">
                    <ul class="list-unstyled">
                        <li><strong>Address Name , Location</strong></li>
                        <li><span>P:</span> +092738237 , +092738237</li>
                        <li><span>E:</span> <a href=""> mail@mail.com</a></li>
                    </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="text-right">
                    <ul class="list-unstyled">
                        <li><strong>Address Name , Location</strong></li>
                        <li><span>P:</span> +092738237 , +092738237</li>
                        <li><span>E:</span> <a href=""> mail@mail.com</a></li>
                    </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'includes/footer.php';?>

        